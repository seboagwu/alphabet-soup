

def rdia(lst1, word):
    # print("rdia:",word)
    cnt = 0
    for i in range(len(lst1)):
        for j in range(len(lst1[i])):
            if i == j:
                lst.append(lst1[i][j])
                if str.__contains__(word, "".join(lst)) and lst1[i][j] == word[cnt]:
                    cnt += 1
                    if len(lst) == 1:
                        out.append(f"{i}:{j}")

                    elif len(lst) == len(word):
                        out.append(f"{i}:{j}")
                else:
                    lst.pop()

    if len(out) == 2:
        return out
    else:
        out.clear()
        lst.clear()
        cnt = 0
        return ldia(lst1, word)


def ldia(lst1, word):
    cnt = 0
    cnt2 = 0
    for i in range(len(lst1)):
        for j in range(len(lst1[i]) -1, 0, -1):
            lst.append(lst1[i][j-cnt2])
            if str.__contains__(word, "".join(lst)) and lst1[i][j-cnt2] == word[cnt]:
                cnt += 1
                if len(lst) == 1:
                    out.append(f"{i}:{j-cnt2}")

                elif len(lst) == len(word):
                        out.append(f"{i}:{j-cnt2}")
            else:
                lst.pop()
            cnt2 += 1
            break

    if len(out) == 2:
        return out
    else:
        out.clear()
        lst.clear()
        cnt = 0
        cnt2 = 0
        return search_right(lst1, word)



def search_right(lst1, word):
    cnt = 0
    for i in range(len(lst1)):
        for j in range(len(lst1[i])):
           if len(lst) >= 1 and lst[0] == lst1[i][j]:
               out[0] = f"{i}:{j}"
           lst.append(lst1[i][j])
           # print("right: ",lst)
           if str.__contains__(word, "".join(lst)) and lst1[i][j] == word[cnt]:
               cnt += 1
               if len(lst) == 1:
                   out.append(f"{i}:{j}")

               elif len(lst) == len(word):
                   out.append(f"{i}:{j}")
           else:
               lst.pop()

    if len(out) == 2:
        return out
    else:
        out.clear()
        lst.clear()
        cnt = 0
        return search_left(lst1, word)


def search_left(lst1, word):
    revlst = []
    for i in range(len(lst1)):
        revlst.append(lst1[i][::-1])
    # print(revlst)

    cnt = 0
    for i in range(len(revlst)):
        for j in range(len(revlst[i])):
            # if len(lst) >= 1 and lst[0] == revlst[i][j]:
            #     out[0] = f"{i}:{j}"
            lst.append(revlst[i][j])
            # print("left: ", lst,out)
            if str.__contains__(word, "".join(lst)) and revlst[i][j] == word[cnt]:
                cnt += 1
                if len(lst) == 1:
                    out.append(f"{i}:{j}")

                elif len(lst) == len(word):
                    out.append(f"{i}:{j}")
            else:
                lst.pop()

    if len(out) == 2:
        return out[::-1]
    else:
        out.clear()
        lst.clear()
        cnt = 0
        return None

## ALL THE Inputs:
# Including File Handling
lst = []
out = []

# Python code to illustrate split() function
with open("./puzzle.txt", "r") as file:      # Change your file path here
    data = file.readlines()
    row = int(data[0][0])
    cnt3 = 0
    lst1 = []
    wordcnt = len(data) - row - 1
    wordlst = []
    for line in data:
        word1 = line.split()
        if cnt3 == 0:
            cnt3 += 1
        elif cnt3 <= row:
            lst1.append(word1)
            cnt3 += 1
        elif cnt3 > row:
            wordlst.append(word1[0])

# Print the Output
for i in wordlst:
    print(f"{i}",*rdia(lst1, i))
    out.clear()